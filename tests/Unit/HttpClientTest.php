<?php

    namespace Tests\Unit;

    use App\rest\HttpClient;
    use GuzzleHttp\Client;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\StreamInterface;
    use Tests\TestCase;

class HttpClientTest extends TestCase
{

    /**
     * @var HttpClient
     */
    private $client;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $guzzleClient;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $response;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $stream;
    /**
     * @var array
     */
    private $dummyData;
    protected $baserUrl;

    protected function setUp(): void
    {
        $this->baserUrl = "abc.com.au";
        $this->guzzleClient = $this->getMockBuilder(Client::class)
            ->setMethods(['get', 'delete', 'post', 'patch', 'put'])->getMock();
        $this->client = new HttpClient($this->guzzleClient, $this->baserUrl);

        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);

        $this->dummyData = ['id' => 10, 'data' => 'test'];
    }

    public function testGetReturnsArray()
    {
        $this->guzzleClient
            ->expects($this->once())
            ->method('get')
            ->willReturn($this->response);

        $this->response
            ->expects($this->once())
            ->method('getBody')
            ->willReturn($this->stream);

        $this->stream
            ->expects($this->once())
            ->method('getContents')
            ->willReturn(json_encode($this->dummyData));

        $res = $this->client->get("todo");

        $this->assertIsArray($res);
    }

    public function testDelete()
    {
        $this->guzzleClient
            ->expects($this->once())
            ->method('delete')
            ->willReturn($this->response);

        $this->response
            ->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);

        $res = $this->client->delete('todos', 10);

        $this->assertEquals(200, $res);
    }

    public function testPost()
    {
        $this->guzzleClient
            ->expects($this->once())
            ->method('post')
            ->willReturn($this->response);

        $res = $this->client->post("todo", [
            'name' => 'ali',
            'age' => 20
        ]);

        $this->assertInstanceOf(ResponseInterface::class, $res);
    }

    public function testPatch()
    {
        $this->guzzleClient
            ->expects($this->once())
            ->method('patch')
            ->willReturn($this->response);

        $res = $this->client->patch("todo", 10, [
            'name' => 'ali',
            'age' => 20
        ]);

        $this->assertInstanceOf(ResponseInterface::class, $res);
    }

    public function testPut()
    {
        $this->guzzleClient
            ->expects($this->once())
            ->method('put')
            ->willReturn($this->response);

        $res = $this->client->put("todo", 10, [
            'name' => 'ali',
            'age' => 20
        ]);

        $this->assertInstanceOf(ResponseInterface::class, $res);
    }

    public function testIsEnabled()
    {
        $this->assertFalse($this->client->isEnableCaching());
    }

    public function testEnableCaching()
    {
        $this->client->setEnableCaching(true);
        $this->assertTrue($this->client->isEnableCaching());
    }

    public function tetGetClient()
    {
        $this->assertInstanceOf(ResponseInterface::class, $this->client->getClient());
    }

    public function testSetClient()
    {
        $this->assertInstanceOf(HttpClient::class, $this->client->setClient($this->response));
        $this->assertInstanceOf(ResponseInterface::class, $this->client->getClient());
    }
}
