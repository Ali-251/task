<?php

namespace App\Http\Controllers;

use App\rest\HttpClientInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class TodosController extends Controller
{
    /**
     * @var \App\rest\HttpClientInterface $client
     */
    private $client;
    /**
     * @var string $object object
     */
    private $object;

    /**
     * TodosController constructor.
     * @param HttpClientInterface $client http client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
        $this->object = config('objects.todo');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view("todo.index")->with(['todos' => $this->client->get($this->object)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view("todo.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request request object
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $data = [
            'task' => $request->get('task', null),
            'done' => (bool)$request->get('done', false),
        ];

        $this->client->post($this->object, $data);

        \request()->session()->flash("message", "Task created");

        return Redirect::route("todo.index");
    }

    /**
     * Display the specified resource.
     * @param int $id id
     * @return Response
     */
    public function show($id)
    {
        $task = $this->client->get($this->object, $id);

        return view("todo.show")->with(['todo' => $task]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id id
     * @return Response
     */
    public function edit($id)
    {
        $data = $this->client->get($this->object, $id);
        $task = null;
        if (isset($data)) {
            $task = $data;
        }

        return view("todo.edit")->with(['todo' => $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request Request object
     * @param int $id id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = [
            'task' => $request->get('task', null),
            'done' => (bool)$request->get('done', false),
        ];

        $this->client->patch($this->object, $id, $data);
        \request()->session()->flash("message", "Task updated");

        return Redirect::route("todo.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $this->client->delete($this->object, $id);
        \request()->session()->flash("message", "Task deleted");

        return Redirect::route("todo.index");
    }
}
