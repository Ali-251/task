<?php

namespace App\Providers;

use App\rest\HttpClient;
use App\rest\HttpClientInterface;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class HttpClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HttpClientInterface::class, function () {
            $config = [
                'headers' => [
                    'x-api-key' => config('apigateway.key'),
                    'User-Agent' => null,
                    'Content-Type' => 'application/json',
                ],
            ];
            $client = new Client($config);
            $baseUrl = env('BASE_URL', "https://k9fhi1kp63.execute-api.us-east-2.amazonaws.com/Todo_dev");

            return new HttpClient($client, $baseUrl);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
