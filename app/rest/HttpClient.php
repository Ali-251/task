<?php

namespace App\rest;

use App\CacheInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class HttpClient implements HttpClientInterface
{
    /**
     * HttpClient constructor.
     */
    /**@var Client $client *
     * private $client;
     * /**@var string
     */
    protected $baseUrl;
    /**
     * @var bool
     */
    protected $enableCaching;
    /**
     * @var CacheInterface
     */
    protected $cache;
    /**
     * @var Client
     */
    protected $client;

    /**
     * HttpClient constructor.
     * @param Client $client Client
     * @param string $baseUrl base url
     * @param bool $enableCache enableCache
     */
    public function __construct(Client $client, $baseUrl, $enableCache = false)
    {
        $this->baseUrl = $baseUrl;
        $this->client = $client;
        $this->enableCaching = $enableCache;
    }

    /**
     * @param string $object object
     * @param int|null $id id
     * @return array
     */
    public function get(string $object, int $id = null): array
    {
        $url = $this->formatUrl($object, $id);
        $res = $this->client->get($url);

        return json_decode($res->getBody()->getContents(), true);
    }

    /**
     * @param string $object object
     * @param int|null $id id
     * @return string
     */
    private function formatUrl($object, $id = null)
    {
        if ($id) {
            return $this->baseUrl . '/' . $object . '/' . $id;
        }

        return $this->baseUrl . '/' . $object;
    }

    /**
     * @param string $object object
     * @param int $id id
     * @return int
     */
    public function delete(string $object, int $id): int
    {
        $url = $this->formatUrl($object, $id);

        return $this->client->delete($url)->getStatusCode();
    }

    /**
     * @param string $object object
     * @param array $data data
     * @return ResponseInterface
     */
    public function post(string $object, array $data): ResponseInterface
    {
        $url = $this->formatUrl($object);

        $body = [
            'body' => json_encode($data),
        ];

        return $this->client->post($url, $body);
    }

    /**
     * @param string $object object
     * @param int $id id id
     * @param array $data data
     * @return ResponseInterface
     */
    public function patch(string $object, int $id, array $data): ResponseInterface
    {
        $url = $this->formatUrl($object, $id);

        return $this->client->patch($url, ['body' => json_encode($data)]);
    }

    /**
     * @param string $object object
     * @param int|null $id id
     * @param array $data data
     * @return ResponseInterface
     */
    public function put(string $object, int $id, array $data): ResponseInterface
    {
        $url = $this->formatUrl($object, $id);
        $body = [
            'body' => json_encode($data),
        ];

        return $this->client->put($url, [$body]);
    }

    /**
     * @return bool
     */
    public function isEnableCaching(): bool
    {
        return $this->enableCaching;
    }

    /**
     * @param bool $enableCaching enableCaching
     * @return HttpClient
     */
    public function setEnableCaching(bool $enableCaching): HttpClient
    {
        $this->enableCaching = $enableCaching;

        return $this;
    }

    /**
     * @return ResponseInterface
     */
    public function getClient(): ResponseInterface
    {
        return $this->client;
    }

    /**
     * @param ResponseInterface $client http client
     * @return HttpClient
     */
    public function setClient(ResponseInterface $client): HttpClient
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl base url
     * @return HttpClient
     */
    public function setBaseUrl(string $baseUrl): HttpClient
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }
}
