<?php


namespace App\rest;

use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface
{
    /**
     * @param string $object object
     * @param int|null $id id
     * @return array
     */
    public function get(string $object, int $id = null): array;

    /**
     * @param string $object object
     * @param array $data data
     * @return ResponseInterface
     */
    public function post(string $object, array $data): ResponseInterface;

    /**
     * @param string $object object
     * @param int $id id id
     * @param array $data data
     * @return ResponseInterface
     */
    public function patch(string $object, int $id, array $data): ResponseInterface;

    /**
     * @param string $object object
     * @param int|null $id id
     * @param array $data data
     * @return ResponseInterface
     */
    public function put(string $object, int $id, array $data): ResponseInterface;

    /**
     * @param string $object object
     * @param int|null $id id
     * @return int
     */
    public function delete(string $object, int $id): int;
}
