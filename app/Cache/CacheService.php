<?php


    namespace App\Cache;

    use Illuminate\Support\Facades\Cache;

class CacheService implements CacheInterface
{

    /**
     * @var string
     */
    private $cacheKey;

    /**
     * CacheService constructor.
     * @param string $cacheKey cache key
     */
    public function __construct(string $cacheKey)
    {
        $this->cacheKey = $cacheKey;
    }

    /**
     * @param string $key key
     * @return array
     */
    public function get(string $key): array
    {
        $finalCacheKey = "$this->cacheKey.{$key}";
        if (Cache::has($finalCacheKey) === false) {
            throw new \http\Exception\InvalidArgumentException();
        }

        return Cache::get($finalCacheKey);
    }

    /**
     * @param string $key key
     * @param mixed $value value
     * @param string $ttl ttle value
     * @return bool
     * @throws \App
     */
    public function set(string $key, $value, $ttl): bool
    {
        Cache::put($key, $key, $ttl);
    }

    /**
     * @param string $key key
     * @return bool
     */
    public function delete(string $key): bool
    {
        return Cache::forget($key);
    }

    /**
     * @return bool
     */
    public function clear(): bool
    {
        return Cache::clearResolvedInstances();
    }

    /**
     * @param string $key key
     * @return bool
     */
    public function has(string $key): bool
    {
        return Cache::has($key);
    }
}
