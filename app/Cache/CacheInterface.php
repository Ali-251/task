<?php

    namespace App\Cache;

interface CacheInterface
{
    /**
     * @param string $key key
     * @return array
     */
    public function get(string $key): array;

    /**
     * @param string $key key
     * @param mixed $value value
     * @param string $ttl ttle value
     * @return bool
     */
    public function set(string $key, $value, string $ttl): bool;

    /**
     * @param string $key key
     * @return bool
     */
    public function delete(string $key): bool;

    /**
     * @return bool
     */
    public function clear(): bool;

    /**
     * @param string $key key
     * @return bool
     */
    public function has(string $key): bool;
}
