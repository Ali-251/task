@extends('layouts.master')

@section("content")
    <h2>Creating a task</h2>

    <form action="{{route("todo.store")}}" method="post">
        @csrf
        <div>
            <h2>Task</h2>
            <textarea name="task"></textarea><br>
            Done: <select name="done">
                <option value=0>No</option>
                <option value=1>yes</option>
            </select>
            <br>

        </div>

        <br>
        <button type="submit" class="btn bg-success">Create</button>
    </form>

@endsection


