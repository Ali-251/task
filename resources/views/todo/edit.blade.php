@extends('layouts.master')

@section('content')
    <h1>Edit Task</h1>
    @if(empty($todo))
        <p>No task to edit</p>

    @else
        <form action="{{ route('todo.update', $todo['id']) }}" method="POST">
            @method('PATCH')
            @csrf
            <table class="table table-stripped">
                <thead>
                <th>No</th>
                <th>Task</th>
                <th>Done</th>
                </thead>
                <tr>
                    <td><textarea name="task">{{$todo['task']}} </textarea></td>
                    <td>
                        <select name="done">
                            <option value=0>No</option>
                            <option value=1>yes</option>
                        </select>
                    </td>
                    </td>
                </tr>
            </table>
            <button class="btn btn-outline-success" type="submit">Save</button>
        </form>
    @endif
@endsection