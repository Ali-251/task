@extends('layouts.master')

@section('content')
    <div class="m-md-2">
        <h1>Todos</h1>
        <a class="btn btn-success float-left" href="{{route("todo.create")}}">Create Task</a>
    </div>
    <table class="table table-stripped">
        <thead>
        <th>Id</th>
        <th>Task</th>
        <th>Done</th>
        <th>Edit</th>
        <th>Delete</th>
        </thead>
        @foreach($todos as $index => $todo)
            <tr>
                <td>{{$todo['id']}}</td>
                <td>{{$todo['task']}}</td>
                <td>{{ $todo['done'] ? 'Yes' : 'No' }}</td>
                <td>
                    <a href="{{route("todo.edit", $todo['id'])}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                    <form method="POST" action="{{route('todo.destroy', $todo['id'])}}"> @csrf  @method('DELETE')
                        <button class="btn btn-danger"> Delete</button>
                    </form>
                </td>

            </tr>
        @endforeach
    </table>
@endsection