@extends('layouts.master')

@section('content')
    <h2>Show page</h2>


    <p>Id: {{ $todo['id'] }}</p>
    <p></p>
    <p>Task: {{ $todo['task'] }}</p>

    <p>Done: {{ $todo['done'] }}</p>
@endsection